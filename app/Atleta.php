<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atleta extends Model
{
    // define campos que podem ser incluídos / alterados na tabela pelos
    // métodos do Laravel
    protected $fillable = array('nome', 'clube', 'salario', 'idade');

    // indica que esta model (tabela produtos) não utiliza os campos 
    // created_at e updated_at
    public $timestamps = false;

}
