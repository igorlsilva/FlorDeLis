<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('sobrenome', 45);
            $table->string('telefone', 45);
            $table->string('genero', 45);
            $table->date('data_nascimento');
            $table->string('endereco', 65);
            $table->string('numero', 5);
            $table->string('complemento', 20);
            $table->string('cep', 45);
            $table->string('email', 191)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('salario');
            $table->date('data_admissao');
            $table->string('funcao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
