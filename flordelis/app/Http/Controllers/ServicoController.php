<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Servico;

class ServicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // recupera todos os registros da tabela servicos
        $dados = Servico::all();

        return view('servicos_lista', ['servicos' => $dados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('servicos_form', ['acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // unique: indica que o campo deve ser único na
        //         tabela servicos
        // min e max: número min e max de caracteres
        // numeric: que o campo deve ser numérico

        $validatedData = $request->validate([
            'nome' => 'required|unique:servicos|min:3|max:100',
            'valor' => 'required|numeric',
            'tempo_duracao' => 'required|min:3|max:10',
            'descricao' => 'required|min:3|max:200'
        ]);

        $dados = $request->all();

        $prod = Servico::create($dados);

        if ($prod) {
            return redirect()->route('servicos.index')
            ->with('status', 'Servico ' . $request->nome . ' inserido com sucesso!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = Servico::find($id);

        return view('servicos_form', ['reg' => $reg, 'acao' => 2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reg = Servico::find($id);

        return view('servicos_form', ['reg' => $reg, 'acao' => 3]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nome' => 'required|unique:servicos,nome,'.$id.'|min:3|max:100',
            'valor' => 'required|numeric',
            'tempo_duracao' => 'required|min:3|max:10',
            'descricao' => 'required|min:3|max:200'
        ]);

        // posiciona no registro a ser alterado
        $reg = Servico::find($id);

        // obtém os campos de formulário
        $dados = $request->all();

        // altera o registro passando os novos dados
        $alt = $reg->update($dados);

        if ($alt) {
          return redirect()->route('servicos.index')
            ->with('status', 'Servico ' . $request->nome . ' alterado com sucesso!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reg = Servico::find($id);
        if ($reg->delete()) {
            return redirect()->route('servicos.index')
            ->with('status', 'Servico ' . $reg->nome . ' excluído corretamente!!');
        }
    }

    public function pesq(Request $request) {
        // recupera todos os registros da tabela servicos
        $dados = Servico::where('nome', 'like','%'.$request->palavra.'%')
                          ->orwhere('descricao','like','%'.$request->palavra.'%')
                          ->get();

        return view('servicos_lista', ['servicos' => $dados,
                         'palavra' => $request->palavra]);
    }
}
