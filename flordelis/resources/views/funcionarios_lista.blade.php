@extends('adminlte::page')

@section('title', 'Funcionarios')

@section('content_header')

<div class="container">

  <div class="row" style="margin-top: 10px">
  <div class="col-sm-6">
    <h2>Cadastro de funcionarios</h2>
  </div>

  <div class="col-sm-4">
     <form method="POST"
           class="form-inline"
           action="{{ route('funcionarios.pesq') }}">
       {{ csrf_field() }}
       <input type="text" class="form-control"
              name="palavra"
              placeholder="Palavra do filtro"> &nbsp;
       <input type="submit" class="btn btn-success"
              value="Ok">
     </form>
  </div>

  <div class="col-sm-2">
      <a href="{{ route('funcionarios.index') }}"
         class="btn btn-warning" role="button">
          Todos</a>

      <a href="{{ route('funcionarios.create') }}"
         class="btn btn-info" role="button">
           Novo</a>
  </div>

  </div>

  @if (session('status'))
  <div class="alert alert-success">
      {{ session('status') }}
  </div>
  @endif

  <table class="table table-hover">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Sobrenome</th>
        <th>Telefone</th>
        <th>E-mail</th>
        <th>Endereço</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($funcionarios as $u)
        <tr>
          <td>{{$u->nome}}</td>
          <td>{{$u->sobrenome}}</td>
          <td>{{$u->telefone}}</td>
          <td>{{$u->email}}</td>
          <td>{{$u->endereco}}</td>
          <td>
           <a href="{{ route('funcionarios.show', $u->id) }}"
              class="btn btn-success btn-sm" role="button">Consultar</a>

           <a href="{{ route('funcionarios.edit', $u->id) }}"
              class="btn btn-warning btn-sm" role="button">Alterar</a>

           <form method="POST" action="{{ route('funcionarios.destroy', $u->id) }}"
                 style="display: inline-block;"
                 onsubmit="return confirm('Confirma Exclusão?') ">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
              <button type="submit" class="btn btn-danger btn-sm">
                Excluir</button>
           </form>

          </td>
        </tr>
      @empty
        <tr><td colspan=5>
          Não há funcionarios cadastrados
          ou com a palavra <b>{{ $palavra or '' }}</b>
          informada na pesquisa </td></tr>
      @endforelse
    </tbody>
  </table>

</div>

@stop
