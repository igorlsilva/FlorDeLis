<?php

use Illuminate\Database\Seeder;

class ServicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('servicos')->insert([
        'nome' => 'Massagem Rexante',
        'descricao' => 'Descrição da massagem relaxante...',
        'valor' => '40',
        'tempo_duracao' => '1 hora',
      ]);

      DB::table('servicos')->insert([
      'nome' => 'Massagem Modeladora',
      'descricao' => 'Descrição da massagem modeladora...',
      'valor' => '50',
      'tempo_duracao' => '1 hora',
      ]);

      DB::table('servicos')->insert([
    'nome' => 'Banho de Lua',
    'descricao' => 'Descrição do banho de lua...',
    'valor' => '40',
    'tempo_duracao' => '1 hora',
      ]);
}
}
