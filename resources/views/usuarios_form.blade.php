@extends('adminlte::page')

@section('title', 'Clientes')
@section('css')
<style media="screen">
  .container{
    max-width: 100%;
  }
</style>
@stop
@section('content')
<div class="container">

  <div class="row" style="margin-top: 10px">

  @if ($errors->any())
    <div class="col-sm-12 alert alert-danger">
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif

  <div class="col-sm-11">

  @if ($acao == 1)
    <h2>Inclusão de Usuarios</h2>
  @elseif ($acao == 2)
    <h2>Consulta de Usuarios</h2>
  @else
    <h2>Alteração de Usuarios</h2>
  @endif

  </div>
  <div class="col-sm-1">
  <a href="{{ route('usuarios.index') }}" class="btn btn-info" role="button">
    Voltar</a>
  </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
  @if ($acao == 1)
    <form action="{{ route('usuarios.store') }}" method="POST">
  @elseif ($acao == 3)
    <form action="{{ route('usuarios.update', $reg->id) }}" method="POST">
    {{ method_field('PUT') }}
  @endif

    {{ csrf_field() }}

    <div class="form-group">
      <label for="nome">Nome:</label>
      <input type="text" class="form-control" id="nome" name="nome"
             value="{{ $reg->nome or old('nome') }}">
    </div>

    <div class="form-group">
      <label for="sobrenome">Sobrenome:</label>
      <input type="text" class="form-control" id="sobrenome" name="sobrenome"
             value="{{ $reg->sobrenome or old('sobrenome') }}">
    </div>

    <div class="form-group">
      <label for="telefone">Telefone:</label>
      <input type="text" class="form-control telefone-mask" id="telefone" name="telefone"
             value="{{ $reg->telefone or old('telefone') }}">
    </div>

    <div class="form-group">
      <label for="genero">Gênero:</label>
      <select name="genero" id="genero" class="form-control">
          <option value="feminino" {{old('genero') == 'feminino' ? 'selected': ''}}>Feminino</option>
          <option value="masculino" {{old('genero') =='masculino' ? 'selected' : ''}}>Masculino</option>
      </select>
    </div>

    <div class="form-group">
      <label for="data_nascimento">Data de Nascimento:</label>
      <input type="date" class="form-control" id="data_nascimento" name="data_nascimento"
             value="{{ $reg->data_nascimento or old('data_nascimento') }}">
    </div>

    <div class="form-group">
      <label for="endereco">Endereço:</label>
      <input type="text" class="form-control" id="endereco" name="endereco"
             value="{{ $reg->endereco or old('endereco') }}">
    </div>

    <div class="form-group">
      <label for="numero">Número:</label>
      <input type="text" class="form-control" id="numero" name="numero"
             value="{{ $reg->numero or old('numero') }}">
    </div>

    <div class="form-group">
      <label for="complemento">Complemento:</label>
      <input type="text" class="form-control" id="complemento" name="complemento"
             value="{{ $reg->complemento or old('complemento') }}">
    </div>

    <div class="form-group">
      <label for="cep">Cep:</label>
      <input type="text" class="form-control cep" id="cep" name="cep"
             value="{{ $reg->cep or old('cep') }}">
    </div>

    <div class="form-group">
      <label for="email">E-mail:</label>
      <input type="text" class="form-control" id="email" name="email"
             value="{{ $reg->email or old('email') }}">
    </div>

    <div class="form-group">
      <!-- <label for="password" class="col-md-4 control-label">Senha:</label> -->
      <label for="password">Senha:</label>
      <input type="password" class="form-control" id="password" name="password"
             value="{{ $reg->password or old('password') }}">
    </div>

    @if($acao == 1 or $acao == 3)
      <button type="submit" class="btn btn-primary">Enviar</button>
      <button type="reset" class="btn btn-success">Limpar</button>
    @endif

  </form>
</div>
</div>
</div
@stop

@section('js')
<script       src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="{{ asset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

$('.telefone-mask').mask(SPMaskBehavior, spOptions);
});
$('.cep').mask('00000-000');

</script>
@stop
