@extends('adminlte::page')

@section('title', 'Administrativo - Home')

@section('content_header')
<h1>Informações do Dia</h1>
@stop
@section('css')
<style media="screen">
  .content-wrapper{
    background: #fff;
  }
</style>
@stop

@section('content')

<div class="container">
  <div class="col-sm-6">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Servicos', 'Nº Clientes'],
      ['Massagem Relaxante', 3],
      ['Drenagem Linfática', 2],
      ['Lipo sem Corte', 2],
      ['Pump Up', 2],

    ]);

    var options = {
      title: 'Nº de Serviços por Cliente',
      is3D: true,
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    chart.draw(data, options);
  }
</script>

<div id="piechart_3d" style="width: 100%; height: auto;"></div>
</div>

<div class="col-sm-6">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Nome');
        data.addColumn('number', 'Valor do Procedimento');
        data.addRows([
          ['Camila',  {v: 70, f: 'R$ 70,00'}],
          ['Nathaly',   {v:180,   f: 'R$180,00'}],
          ['Alice', {v: 100, f: 'R$100,00'}],
          ['Júlia',   {v: 230,  f: 'R$230,00'}]
        ]);


        var table = new google.visualization.Table(document.getElementById('table_div'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>

    <div id="table_div" style="width: 500px; height: 200px;"></div>

</div>
</div>
<div class="container" style="margin-top: 30px;">
  <h3>Informações da Semana</h3>

<div class="col-sm-6">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
          var data = google.visualization.arrayToDataTable([
            ['Serviço', 'Clientes'],
            ['Radiofrequência',     7],
            ['Massagem Detox',      8],
            ['Banho de Lua',  1],
            ['Pump Up', 4],
            ['Drenagem Linfática',    3],
            ['Massagem Modeladora', 14],
            ['Lipo', 2],
            ['Limpeza de pele', 9],
            ['Peeling', 6],
            ['Massagem Relaxante', 2]
          ]);

          var options = {
            title: 'Atendimentos da Semana',
            pieHole: 0.4,
          };

          var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
          chart.draw(data, options);
        }
      </script>

      <div id="donutchart" style="width: 100%; height: 100%;"></div>



</div>
<div class="col-sm-6">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Dia', 'Serviços'],
          ['Seg',  220],
          ['Ter',  122],
          ['Qua',  340],
          ['Qui',  330],
          ['Sex',  500],
          ['Sab',  60],
        ]);

        var options = {
          title: 'Faturamento da Semana',

        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>


  <div id="chart_div" style="width: 100%; height: auto;"></div>

</div>
</div>
@stop
